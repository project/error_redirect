<?php

/**
 * @file
 * Defines form and page render functions.
 */

/**
 * Error redirect list form.
 */
function error_redirect_list_form() {
  // Loads all redirects.
  $redirects = error_redirect_load_multiple();

  $form['#tree'] = TRUE;
  $form['redirects'] = array();

  foreach ($redirects as $rid => $redirect) {
    // Add the redirect fields.
    foreach (error_redirect_fields() as $field => $label) {
      $form['redirects'][$rid][$field] = array('#markup' => nl2br(htmlspecialchars($redirect->$field)));
    }

    // Add weight field.
    $form['redirects'][$rid]['weight'] = array(
      '#type' => 'weight',
      '#title' => t('Weight'),
      '#title_display' => 'invisible',
      '#default_value' => $redirect->weight,
      '#attributes' => array(
        'class' => array('weight'),
      ),
    );

    // Add operations.
    $form['redirects'][$rid]['operations'] = array(
      '#markup' => theme('item_list', array(
        'items' => array(
          l(t('edit'), "admin/config/search/error_redirect/$rid/edit"),
          l(t('delete'), "admin/config/search/error_redirect/$rid/delete"),
        ),
        'attributes' => array(
          'class' => array('links', 'inline', 'nowrap'),
        ),
      )),
    );
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save changes'),
  );

  return $form;
}

/**
 * Error redirect list form submit.
 */
function error_redirect_list_form_submit($form, &$form_state) {
  if (isset($form_state['values']['redirects'])) {
    foreach ($form_state['values']['redirects'] as $rid => $data) {
      // Set new weight for redirect and save it.
      $redirect = new stdClass();
      $redirect->rid = $rid;
      $redirect->weight = $data['weight'];

      error_redirect_save($redirect);
    }
  }
}

/**
 * Error redirect form.
 */
function error_redirect_form($form, &$form_state, $redirect = NULL) {
  $form_state['error_redirect'] = $redirect;

  $form['rid'] = array(
    '#type' => 'value',
    '#value' => isset($redirect->rid) ? $redirect->rid : NULL,
  );

  $form['weight'] = array(
    '#type' => 'weight',
    '#title' => t('Weight'),
    '#default_value' => isset($redirect->weight) ? $redirect->weight : 0,
  );

  $form['wildcards'] = array(
    '#type' => 'textarea',
    '#title' => t('Wildcards'),
    '#default_value' => isset($redirect->wildcards) ? $redirect->wildcards : NULL,
    '#required' => TRUE,
    '#description' => t("Specify redirect sources by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are blog for the blog page and blog/* for every personal blog. <front> is the front page."),
  );

  $form['target'] = array(
    '#type' => 'textfield',
    '#title' => t('Target'),
    '#default_value' => isset($redirect->target) ? $redirect->target : NULL,
    '#required' => TRUE,
    '#description' => t('The path for redirect target. This can be an internal Drupal path or alias such as node/add or an external URL such as http://drupal.org. Enter <front> to link to the front page.'),
  );

  $form['error_code'] = array(
    '#type' => 'select',
    '#title' => t('Error Code'),
    '#options' => error_redirect_error_codes(),
    '#default_value' => isset($redirect->error_code) ? $redirect->error_code : NULL,
    '#required' => TRUE,
  );

  $form['status_code'] = array(
    '#type' => 'select',
    '#title' => t('Status Code'),
    '#options' => error_redirect_status_codes(),
    '#default_value' => isset($redirect->status_code) ? $redirect->status_code : 301,
    '#required' => TRUE,
  );

  $form['actions'] = array('#type' => 'actions');

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  if (isset($redirect->rid)) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
    );
  }

  return $form;
}

/**
 * Error redirect form validate.
 */
function error_redirect_form_validate($form, &$form_state) {
  // Don't allow target to be within it's own pattern.
  if (drupal_match_path($form_state['values']['target'], $form_state['values']['wildcards'])) {
    form_set_error('target', t('The wildcards has the target url.'));
  }

  // Don't allow will cause a recurrence.
  $redirects = error_redirect_load_multiple();

  foreach ($redirects as $redirect) {
    if (drupal_match_path($form_state['values']['target'], $redirect->wildcards)
      && drupal_match_path($redirect->target, $form_state['values']['wildcards'])) {
      form_set_error('target', t('It will сause a recurrence with <a href="@url" target="_blank">existing redirect</a>', array(
        '@url' => url('admin/config/search/error_redirect/' . $redirect->rid),
      )));
      break;
    }
  }
}

/**
 * Error redirect form submit.
 */
function error_redirect_form_submit($form, &$form_state) {
  form_state_values_clean($form_state);

  // Save redirect.
  if ($form_state['error_redirect'] == (object) $form_state['values'] || error_redirect_save((object) $form_state['values'])) {
    drupal_set_message(t('The error redirect has been successfully saved.'));
    $form_state['redirect'] = '/admin/config/search/error_redirect';
  }
}

/**
 * Error redirect delete confirm form.
 */
function error_redirect_delete_confirm($form, &$form_state, $redirect) {
  $form_state['error_redirect'] = $redirect;

  return confirm_form($form, t('Are you sure you want to delete the redirect'), 'admin/config/search/error_redirect');
}

/**
 * Error redirect delete confirm form submit.
 */
function error_redirect_delete_confirm_submit($form, &$form_state) {
  // Delete redirect.
  if (error_redirect_delete($form_state['error_redirect'])) {
    drupal_set_message(t('The error redirect has been successfully deleted.'));
    $form_state['redirect'] = '/admin/config/search/error_redirect';
  }
}

/**
 * Theme callback for error redirect list form.
 */
function theme_error_redirect_list_form($variables) {
  // Add tabledrag js.
  drupal_add_tabledrag('error-redirects', 'order', 'sibling', 'weight');

  // Get form.
  $form = $variables['form'];

  // Get fields.
  $fields = error_redirect_fields();

  // Add operation field to fields.
  $fields['operations'] = t('Operations');

  // Generate rows.
  $rows = array();

  foreach (element_children($form['redirects']) as $rid) {
    foreach ($fields as $field => $label) {
      $rows[$rid]['data'][] = drupal_render($form['redirects'][$rid][$field]);
    }
    $rows[$rid]['class'] = array('draggable');
  }

  $output = theme('table', array(
    'header' => $fields,
    'rows' => $rows,
    'empty' => t('There are no redirects.'),
    'attributes' => array(
      'id' => 'error-redirects',
    ),
  ));

  $output .= drupal_render_children($form);

  return $output;
}
