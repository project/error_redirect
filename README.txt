DESCRIPTION
===========
This module provides redirecting to the target page
when the user gets an error (403 or 404) on specific page (can be wildcards).
For example, we have books node type available only for one role (reader).
So, other users will see the 403 error (access denied), but instead this error,
we want to redirect this users to the form where he can get the reader role.
